<?php
if ( ! function_exists( "ctsync_shortcodeHandleEmptyAtts" ) ) {
	function ctsync_shortcodeHandleEmptyAtts( $atts ) {
		foreach ( $atts as $attr => $val ) {
			if ( is_int( $attr ) ) {
				$atts[ strtolower( $val ) ] = true;
				unset( $atts[ $attr ] );
			}
		}

		return $atts;
	}
}

/**
 *Function printCalendarEvents:
 *    Mögliche Interessante Daten zur Ausgabe befinden sich in
 *    $var->bezeichnung, $var->startdate, $var->enddate,
 *    $var->link,$var->ort, $var->notizen
 */
if ( ! function_exists( 'ctsync_sanitize_shortcode' ) ) {
	function ctsync_sanitize_shortcode( $atts ) {
		// Pull in shortcode attributes and set defaults
		$atts = shortcode_atts( array(
			//attributes for calendar-shortcode
			'events_per_page'  => get_option( 'ctsync_options' )['maximport'],
			'style'            => 'default',
			'show_update_info' => false,
			//attributes for both
			'show_category'    => false,
			'offset'           => 0,
			'categories'       => '',
			'title_matches'    => '',
			'display_image'    => false,
			//attributes for event-shortcode
			'component'        => 'event', //title, description, link, startdate, enddate, date, event
			'modal_id'         => 'churchtools_event_popup',
		), ctsync_shortcodeHandleEmptyAtts( $atts ), 'churchtools_cal' );

		$categories     = sanitize_text_field( $atts['categories'] );
		$atts['offset'] = intval( $atts['offset'] );
		// $atts['order']         = sanitize_key( $atts['order'] );
		// $atts['orderby']       = sanitize_key( $atts['orderby'] );
		$atts['events_per_page']  = intval( $atts['events_per_page'] );
		$atts['show_category']    = filter_var( $atts['show_category'], FILTER_VALIDATE_BOOLEAN );
		$atts['show_update_info'] = filter_var( $atts['show_update_info'], FILTER_VALIDATE_BOOLEAN );
		$atts['categories']       = [];
		foreach ( preg_split( '/\D/', $categories ) as $id ) {
			if ( intval( $id ) > 0 ) {
				$atts['categories'][] = intval( $id );
			}
		}
		if ( empty( $atts['categories'] ) ) {
			$option             = get_option( 'ctsync_options' );
			$atts['categories'] = $option['ids'];
		}

		return $atts;
	}
}

if ( ! function_exists( 'ctsync_printEventStartTime' ) ) {
	function ctsync_printEventStartTime( $event ) {
		$startdate = ( new DateTime( $event->calculated->startDate, new DateTimeZone( 'UTC' ) ) )->setTimezone( wp_timezone() );
		$return    = '<time datetime="' . $startdate->format( 'Y-m-d H:i:00' ) . '">';
		$return    .= $startdate->format( 'd.m.Y H:i' );
		$return    .= '</time>';

		return $return;
	}
}
if ( ! function_exists( 'ctsync_printEventEndTime' ) ) {
	function ctsync_printEventEndTime( $event ) {
		$enddate = ( new DateTime( $event->calculated->endDate, new DateTimeZone( 'UTC' ) ) )->setTimezone( wp_timezone() );
		$start   = ( new DateTime( $event->calculated->startDate ) )->setTime( 0, 0, 0 );
		$end     = ( new DateTime( $event->calculated->endDate ) )->setTime( 0, 0, 0 );
		$return  = '<time datetime="' . $enddate->format( 'Y-m-d H:i:00' ) . '">';
		if ( $start == $end ) {
			$return .= $enddate->format( 'H:i' );
		} else {
			$return .= $enddate->format( 'd.m.Y H:i' );
		}
		$return .= '</time>';

		return $return;
	}
}
if ( ! function_exists( 'ctsync_printEventTime' ) ) {
	function ctsync_printEventTime( $event ) {
		return ctsync_printEventStartTime( $event ) . ' - ' . ctsync_printEventEndTime( $event );
	}
}
if ( ! function_exists( 'ctsync_printEventLink' ) ) {
	function ctsync_printEventLink( $event, $wrap_text ) {
		$return = '';
		if ( ! empty( $event->base->link ) ) {
			$return .= '<a href="' . $event->base->link . '">';
			$return .= $wrap_text;
			$return .= '</a>';
		}

		return $return;
	}
}
if ( ! function_exists( 'ctsync_printEventCategory' ) ) {
	function ctsync_printEventCategory( $event, $atts ) {
		if ( $atts['show_category'] ) {
			return '<span class="event_category_name">' . htmlspecialchars( $event->base->calendar->name ) . '</span>';
		} else {
			return '';
		}
	}
}
if ( ! function_exists( 'ctsync_printEventImage' ) ) {
	function ctsync_printEventImage( $event ) {
		return ! empty( $event->base->image ) ? '<img style="display: block; margin: auto" src="' . $event->base->image->fileUrl . '"/>' : '';
	}
}
if ( ! function_exists( 'ctsync_wrapModalLink' ) ) {
	function ctsync_wrapModalLink( $atts, $wrap_text ) {
		if ( $atts['modal_id'] != 'churchtools_event_popup' ) {
			return '<a data-toggle="modal" class="btn btn-primary btn-lg" href="#' . $atts['modal_id'] . '">' . $wrap_text . '</a>';
		} else {
			return $wrap_text;
		}
	}
}
if ( ! function_exists( 'ctsync_printEventShortDesc' ) ) {
	function ctsync_printEventShortDesc( $event ) {
		$notes     = isset( $event->base->information ) ? $event->base->information : '';
		$link_text = Strip_tags( $notes );
		if ( strlen( $link_text ) > 150 ) {
			$pos = strpos( $link_text, ' ', 150 );
			if ( $pos < 150 ) {
				return $link_text;
			} else {
				return substr( $link_text, 0, $pos ) . '...';
			}
		} else {
			return $link_text;
		}
	}
}

if ( ! function_exists( 'ctsync_printCalendarEvent' ) ) {
	function ctsync_printCalendarEvent( $event, $atts ) {
		$return = '';
		switch ( $atts['style'] ) {
			case 'short':
				$return .= '<p class="event">';
				$return .= '<strong>' . ctsync_wrapModalLink( $atts, htmlspecialchars( $event->base->caption ) ) . '</strong><br />';
				$return .= ctsync_printEventTime( $event );
				$return .= '</p>';
				break;
			case 'block':
				$return .= '<div class="event">' . '<div class="wrapper">';
				if ( $atts['display_image'] ) {
					$return .= ctsync_printEventImage( $event );
				}
				$return .= '<div class="title_category">';
				$return .= '<h3 style="text-align: center">' . ctsync_wrapModalLink( $atts, htmlspecialchars( $event->base->caption ) ) . '</h3>';
				$return .= '<div style="text-align: center;">' . ctsync_printEventTime( $event ) . '</div>';
				$return .= '</div>';
				$return .= '<div style="text-align: center;">' . ctsync_printEventCategory( $event, $atts ) . '</div>';
				$return .= '</div>' . '</div>';
				break;
			case 'detail-block':
				$return .= '<div class="event">' . '<div class="wrapper">';
				if ( $atts['display_image'] ) {
					$return .= ctsync_printEventImage( $event );
				}
				$return .= '<div class="title_category">';
				$return .= '<h3 style="text-align: center">' . htmlspecialchars( $event->base->caption ) . '</h3>';
				$return .= '<h4 style="text-align: center">' . ctsync_printEventTime( $event ) . '</h3>';
				$return .= '</div>';
				$return .= '<div style="text-align: center;">' . ctsync_printEventCategory( $event, $atts ) . '</div>';
				$return .= '<div style="text-align: center;">' . ctsync_printEventShortDesc( $event ) . '</div>';
				if ( $atts['modal_id'] == 'churchtools_event_popup' ) {
					$return .= '<div style="text-align: center;">' . ctsync_printEventLink( $event, 'Mehr Infos' ) . '</div>';
				} else {
					$return .= '<div style="text-align: center;">' . ctsync_wrapModalLink( $atts, 'Mehr Infos' ) . '</div>';
				}
				$return .= '</div>' . '</div>';
				break;
			default:
				$return .= '<li class="event">' . '<div class="wrapper">';
				if ( $atts['display_image'] ) {
					$return .= ctsync_printEventImage( $event );
				}
				$return .= '<div class="title_category">';
				$return .= '<h3>' . ctsync_wrapModalLink( $atts, htmlspecialchars( $event->base->caption ) ) . '</h3>';
				$return .= ctsync_printEventCategory( $event, $atts );
				$return .= '</div>';
				$return .= ctsync_printEventTime( $event );
				$return .= '</div>' . '</li>';
				break;
		}

		return $return;
	}
}

if ( ! function_exists( 'ctsync_getEventsMatchingAtts' ) ) {
	function ctsync_getEventsMatchingAtts( $events, $atts ) {
		$return = array();
		$i      = 0;
		foreach ( $events as $event ) {
			if ( in_array( $event->base->calendar->id, $atts['categories'] )
			     && ( empty( $atts['title_matches'] ) || strpos( strtolower( $event->base->caption ), strtolower( $atts['title_matches'] ) ) !== false )
			) {
				if ( $i < $atts['offset'] ) {
					$i ++;
				} else {
					$return[] = $event;
				}
			}
		}

		return $return;
	}
}

/**
 * [churchtools_cal] shortcode
 */
if ( ! function_exists( 'ctsync_printCalendarEvents' ) ) {
	function ctsync_printCalendarEvents( $events, $atts ) {
		function appendEventsToResult( $events, $atts ) {
			$return = '';
			$events = ctsync_getEventsMatchingAtts( $events, $atts );
			if ( empty( $events ) ) {
				//if no matching event was found, print error message
				$return .= "No matching events available";
			} else {
				foreach ( $events as $event ) {
					$return .= ctsync_printCalendarEvent( $event, $atts );
				}
			}

			return $return;
		}

		wp_enqueue_style( 'ctstyle' );
		$return = '<div class="ct-events">' . "\n";
		if ( ! empty( $events ) ) {
			switch ( $atts['style'] ) {
				case 'short':
					$return .= appendEventsToResult( $events, $atts );
					break;
				default:
					if ( count( $atts['categories'] ) == 1 ) {
						$ulClass = 'single_calendar';
					} else {
						$ulClass = 'multi_calendar';
					}
					$return .= '<ul class="' . $ulClass . '">' . "\n";
					$return .= appendEventsToResult( $events, $atts );
					$return .= '</ul>' . "\n";
					break;
			}
		} else {
			$return .= '<p>ChurchTools Fehler! Daten stehen momentan nicht zur Verfügung.</p>' . "\n";
		}
		if ( $atts['show_update_info'] ) {
			$return .= '<div class="last_updated"><strong>Last updated:</strong> ' . get_transient( 'churchtools_calendar_lastupdated' ) . '</div>';
		}
		$return .= '</div>' . "\n";

		return $return;
	}
}

/**
 * [churchtools_evt] shortcode
 */
if ( ! function_exists( 'ctsync_printSingleEvent' ) ) {
	function ctsync_printSingleEvent( $events, $atts, $link_text ) {
		$return    = '';
		$i         = 0;
		$valid_evt = false; //checking if event matches filter or list has gone out of events
		foreach ( $events as $event ) {
			if ( in_array( $event->base->calendar->id, $atts['categories'] )
			     && ( empty( $atts['title_matches'] ) || strpos( strtolower( $event->base->caption ), strtolower( $atts['title_matches'] ) ) !== false )
			) {
				if ( $i < $atts['offset'] ) {
					$i ++;
				} else {
					$valid_evt = true; //loop ends because of valid event
					break;
				}
			}
		}
		$events = ctsync_getEventsMatchingAtts( $events, $atts );
		//if loop ended because of to few events, return content of shortcode or error message
		if ( empty( $events ) ) {
			return $link_text == '' ? "No matching event available" : $link_text;
		}

		$event = $events[0];
		switch ( $atts['component'] ) {
			case 'title':
				$return .= htmlspecialchars( $event->base->caption );
				break;
			case 'description':
				$return .= ! empty( $event->base->information ) ? htmlspecialchars( $event->base->information ) : '';
				break;
			case 'category':
				$return .= ctsync_printEventCategory( $event, $atts );
				break;
			case 'link':
				$return .= ctsync_printEventLink( $event, $link_text );
				break;
			case 'startdate':
				$return .= ctsync_printEventStartTime( $event );
				break;
			case 'endate':
				$return .= ctsync_printEventEndTime( $event );
				break;
			case 'date':
				$return .= ctsync_printEventTime( $event );
				break;
			case 'modal':
				$return .= '<div id="' . $atts['modal_id'] . '" class="modal" tabindex="-1" role="message">' . '<div class="modal-dialog" role="document">' . '<div class="modal-content">';
				$return .= '<div class="modal-header"><button class="close" type="button" data-dismiss="modal">×</button><h4 class="modal-title">' . $event->base->caption . '</h4></div>';
				$return .= '<div class="modal-body">';
				$return .= ctsync_printEventImage( $event );
				$return .= isset( $event->base->information ) ? '<p>' . $event->base->information . '</p>' : '';
				$return .= '<p>' . ctsync_printEventLink( $event, 'Mehr Infos' ) . '</p>';
				$return .= '</div>';
				$return .= '<div class="modal-footer">' . ctsync_printEventTime( $event ) . '</div>' . '</div>' . '</div>' . '</div>';
				break;
			case 'event':
			default:
				$return .= ctsync_printCalendarEvent( $event, $atts );
				break;
		}

		return $return;
	}
}