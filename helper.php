<?php

if ( ! defined( 'CT_URL' ) ) {
	define( 'CT_URL', 'api/calendars/appointments?' );
}

if ( ! function_exists( 'ctsync_sendRequest' ) ) {
	function ctsync_sendRequest( $url, $logintoken, $data ) {
		$header      = "Authorization: Login " . $logintoken;
		$options     = array(
			'http' => array(
				'header' => $header,
				'method' => 'GET',
			)
		);
		$context     = stream_context_create( $options );
		$url         = $url . CT_URL . http_build_query( $data );
		$rsp_headers = get_headers( $url, true, $context );
		if ( ! stripos( $rsp_headers[0], '200' ) ) {
			echo "There is an error:" . print_r( $rsp_headers, true );
			error_log( 'There is an error: ' . print_r( $rsp_headers, true ) );

			return null;
		}
		$result = file_get_contents( $url, false, $context );

		return json_decode( $result );
	}
}

/**
 * Function sorts the ChurchTools-Array by Starttime of the Event.
 */
if ( ! function_exists( 'ctsync_sanitize_data' ) ) {
	function ctsync_sanitize_data( $array ) {
		function filter_events($entry) {
			return !$entry->base->isInternal;
		}

		$array = array_filter($array, "filter_events");

		$dates = array();
		foreach ( $array as $key => $row ) {
			$dates[ $key ] = strtotime( $row->calculated->startDate );
		}
		array_multisort( $dates, SORT_ASC, $array );

		return $array;
	}
}

if ( ! function_exists( 'ctsync_getUpdatedCalendarEvents' ) ) {
	function ctsync_getUpdatedCalendarEvents() {
		/**
		 * Function gets newest CalendarEvents from ChurchTools and saves them to Transient.
		 * Returns as Array
		 */
		$options = get_option( 'ctsync_options' );
		if ( empty( $options ) || empty( $options['url'] ) ) {
			return null;
		}
		//define options for request
		$data   = array(
			'calendar_ids' => $options['ids'],
			'from'         => current_time( 'Y-m-d' ),
			'to'           => ( new DateTimeImmutable( '+' . $options['maximport'] . ' days', wp_timezone() ) )->format( 'Y-m-d' )
		);
		$result = ctsync_sendRequest( $options['url'], $options['logintoken'], $data );
		if ( $result == null ) {
			return null;
		}
		//save data
		if ( ! empty( $result ) ) {
			set_transient( 'churchtools_calendar', ctsync_sanitize_data( $result->data ), 24 * HOUR_IN_SECONDS );
			set_transient( 'churchtools_calendar_lastupdated', current_time( 'mysql' ), 24 * HOUR_IN_SECONDS );

			return $data;
		} else {
			return null;
		}
	}
}
if ( ! function_exists( 'ctsync_getCalendarEvents' ) ) {
	/**
	 * Function returns CalendarEvents as an Array
	 */
	function ctsync_getCalendarEvents() {
		if ( false === ( $result = get_transient( 'churchtools_calendar' ) ) ) {
			$result = ctsync_getUpdatedCalendarEvents();
		}

		return $result;
	}
}